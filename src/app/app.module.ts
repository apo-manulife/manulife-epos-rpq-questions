import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

// load components
import { AppComponent } from './app.component';

// load modules
import { AppRoutingModule } from './app-routing.module';
import { QuestionsComponent } from './components/pages/questions/questions.component';
import { NavbarComponent } from './components/sections/navbar/navbar.component';
import { QuestionsService } from './services/questions.service';

// load epos materials
import { eposTheme } from '../assets/manulife-epos-theme/eposTheme.module';

// HammerGestureConfig
declare let Hammer:any;
import { HammerGestureConfig, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import * as Hammer from 'hammerjs';
export class customHammerConfig extends HammerGestureConfig  {
  overrides = <any>{
    'pinch': { enable: false },
    'rotate': { enable: false },
    'press': { enable: false },
    'doubletap': { enable: false },
  };
  buildHammer(element: HTMLElement) {
    let hammertime = new Hammer(element, {
      touchAction: 'pan-y',
      inputClass: Hammer.TouchInput,
    });
    hammertime.on( 'tap', (event)=>{
      event.preventDefault();
    });
    return hammertime;
  }
}

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    eposTheme.forRoot(),
  ],
  declarations: [
    AppComponent,
    QuestionsComponent,
    NavbarComponent,
  ],
  providers: [
    QuestionsService,
    { 
      provide: HAMMER_GESTURE_CONFIG, 
      useClass: customHammerConfig,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
