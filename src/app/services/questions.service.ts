import { Injectable } from '@angular/core';
import { BehaviorSubject }    from 'rxjs/BehaviorSubject';
import { TweenLite } from "gsap";


export const MockData:any[] = [
  {
      inputtype: "select",
      message: "What is your age?",
      options: [
          "> 55",
          "45-55",
          "35-45",
          "18-35",
      ],
  },
  {
      inputtype: "select",
      message: "How about your income?",
      options: [
          "Very unstable",
          "Unstable",
          "Stable",
          "Very stable",
      ],
  },
  {
      inputtype: "select",
      message: "How long can you pay for an invest - insurance policy?",
      options: [
          "< 1 year",
          "1 - 5 years",
          "6 - 10 years",
          "11 - 20 years",
      ],
  },
  {
      inputtype: "select",
      message: "Do you have experience in securities investment?",
      options: [
          "Inexperience",
          "< 1 year",
          "1 - 5 years",
          "> 5 years",
      ],
  },
  {
      inputtype: "select",
      message: "How many percent of income do you need from this investment for your daily living expenses?",
      options: [
          "25 - 50%",
          "10 - 25%",
          "0 - 10%",
          "0%",
      ],
  },
  {
      inputtype: "select",
      message: "What is your investment expectation?  ",
      image: "./assets/images/invest_chart.png",
      options: [
          "-5%/+5%",
          "-10%/+10%",
          "-25%/+25%",
          "-40%/+40%",
      ],
      cssClass: "investment-expectation",
  },
  {
      inputtype: "select",
      message: "What will you do if your investmen lost 20%?",
      options: [
          "Sell all remaining investments",
          "Sell a portion of the investment",
          "Still continue",
          "Change the investment list ",
      ],
      optionsalign: "list",
  },
  {
      inputtype: "select",
      message: "Which of the following is the best description of your views on investing?",
      options: [
          "Slow and steady",
          "Accept a little extra risk to increase profits",
          "Accept low losses to seek investment opportunities",
          "Accept high risk to increase profitability",
      ],
      optionsalign: "list",
  },
  {
      inputtype: "select",
      message: "If you are rewarded with 400 million, how do you invest this amount?",
      options: [
          "Cash",
          "Deposit",
          "Gold",
          "Securities",
      ],
  },
  {
      inputtype: "select",
      message: "How willing are you to take risk?",
      options: [
          "I only accept minimal risk and profitability may be relatively low",
          "I can accept low risk for the opportunity of higher profit in the long term ",
          "I can accept medium risk for the opportunity of higher profit in the long term",
          "I can accept high risk for a chance to make a significant profit in the long term ",
      ],
      optionsalign: "list",
  },
]

@Injectable()
export class QuestionsService {
  public questions = MockData.splice(0);
  private currentQuestion;
  public currentQuestionChange: BehaviorSubject<any> = new BehaviorSubject<any>(0);

  constructor(
  ) { 
    this.currentQuestionChange.subscribe(data => {
      this.currentQuestion = data;
    });
  }

  setCurrentQuestion(data) {
    this.currentQuestionChange.next(data);
  }

  scrollToTop(scrollEle, topEle=undefined) {
    if (!topEle)
        TweenLite.to(scrollEle.nativeElement, .5, {scrollTop: 0});
    else
        this.tweenScrollTop(scrollEle, topEle.nativeElement.offsetTop-scrollEle.nativeElement.offsetTop);
  }

  scrollToBottom(scrollEle) {
    this.tweenScrollTop(scrollEle, scrollEle.nativeElement.scrollHeight);
  }

  tweenScrollTop(scrollEle, scrollTop){
    setTimeout(() => {
        TweenLite.to(
            scrollEle.nativeElement, 
            .4, 
            {scrollTop: scrollTop}
        );
    }, 150);
  }

}
