import { Component, OnInit, OnDestroy, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

export class NavbarItem {
  id: string;
  svgIcon: string;
}

export class NavbarItems {
  id: string;
  items: NavbarItem[];
}

export const NavbarItemsList:NavbarItems[] = [
  {
      id: "questions",
      items: [
          { id: "tools", svgIcon: "epos:tools" },
      ]
  },
]

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})

export class NavbarComponent implements OnInit, OnDestroy {
  searchInput:string;
  private currentQuestion;
  private currentQuestionSubscription: Subscription;
  constructor(
    private router: Router,
  ) {
  }

  ngOnInit() {
    // this.currentQuestionSubscription = this.proposalService.currentQuestionChange.subscribe(product => {
    //   this.currentQuestion = product;
    // });
  }

  ngOnDestroy() {
    this.currentQuestionSubscription.unsubscribe();
  }

  getNavItems(key) {
    let result = NavbarItemsList.find(item=> item.id == key);
    if (!result) return [];
    return result.items;
  }

  navitemClick(event, item) {
    let eleref = new ElementRef(event.target);
    switch(item.id) {
      case "tools":
        break;
      default:
        break;
    }
  }
}
