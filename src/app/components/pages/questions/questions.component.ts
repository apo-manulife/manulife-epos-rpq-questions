import { Component, OnInit, ViewChild, ViewChildren, QueryList, ElementRef } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import {QuestionsService} from '../../../services/questions.service';

@Component({
  selector: 'app-questions',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.scss'],
})
export class QuestionsComponent implements OnInit {
  questionStepper;
  answered = 0;
  currentQuestion = 0;
  private currentQuestionSubscription: Subscription;
  @ViewChild('scrollcontent') private scrollcontent;
  @ViewChildren('questionbox') private questionbox:QueryList<ElementRef>;

  constructor(
    public questionsService: QuestionsService,
  ) { 
    this.currentQuestionSubscription = this.questionsService.currentQuestionChange.subscribe(data => {
      this.currentQuestion = data;
      setTimeout(() => {
        this.answered = this.getAnswered();
        var quests = this.questionbox.toArray();
        if (quests.length <= this.currentQuestion) {
          this.questionsService.scrollToBottom(this.scrollcontent)
        }
        else if (quests.length > this.currentQuestion)
          this.questionsService.scrollToTop(this.scrollcontent, quests[this.currentQuestion]);
      });
    });
  }

  ngOnInit() {}

  getAnswered() {
    var result = this.questionsService.questions.filter(item=>{ 
      return item.answer!=undefined;
    });
    if (result==undefined) return 0;
    return result.length;
  }

  quest_change(idx, quest, answer){
    quest.answer = answer;
    this.questionsService.setCurrentQuestion(idx+1);
  }

  checkValid() {
    var result = this.questionsService.questions.find(item=>{ 
      return item.answer==undefined;
    });
    return result!=undefined;
  }

}
