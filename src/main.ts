import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
// import * as FastClick from 'fastclick';

if (environment.production) {
  enableProdMode();
}

const bootstrap = () => {
  platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.log(err));
};

if (window['cordova']) {
  document.addEventListener('deviceready', () => {
    // FastClick.attach(document.body);
    // window['_app_base'] = '/' + window.location.pathname.split('/')[1];
    bootstrap();
  }, false);
} else {
  bootstrap();
}