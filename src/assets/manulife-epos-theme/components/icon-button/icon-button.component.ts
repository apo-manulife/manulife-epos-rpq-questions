import { Component, OnInit, Input, Output, ViewEncapsulation, EventEmitter, ElementRef } from '@angular/core';

@Component({
  encapsulation: ViewEncapsulation.None,
  selector: 'epos-icon-button',
  templateUrl: './icon-button.component.html',
  styleUrls: ['./icon-button.component.scss'],
})
export class IconButtonComponent implements OnInit {
  @Input() svgIcon: string;
  @Input() label: string;
  @Input() svgColor: string;
  @Input() disabled: boolean;

  constructor(
    private elementRef:ElementRef, 
  ) { 
    this.elementRef.nativeElement.style.display = "flex";
  }

  ngOnInit() { 

  }

  ngOnChanges(changes){
    this.elementRef.nativeElement.style['pointer-events'] = (this.disabled)? "none" : "auto"; 
  }
}
