import { Injectable, Optional } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material';

export class IconSvgRegistryConfig {
  icons = [];
}

export const IconSvgRegistryList:any = [
  {
    name: 'menu', 
    url: 'assets/manulife-epos-theme/images/menu.svg',
  },
  {
    name: 'back', 
    url: 'assets/manulife-epos-theme/images/back.svg',
  },
  {
    name: 'close', 
    url: 'assets/manulife-epos-theme/images/close.svg',
  },
  {
    name: 'search', 
    url: 'assets/manulife-epos-theme/images/search.svg',
  },
  {
    name: 'compare', 
    url: 'assets/manulife-epos-theme/images/compare.svg',
  },
  {
    name: 'tools', 
    url: 'assets/manulife-epos-theme/images/tools.svg',
  },
  {
    name: 'setting', 
    url: 'assets/manulife-epos-theme/images/setting.svg',
  },
  {
    name: 'customer', 
    url: 'assets/manulife-epos-theme/images/customer.svg',
  },
  {
    name: 'export', 
    url: 'assets/manulife-epos-theme/images/export.svg',
  },
  {
    name: 'save', 
    url: 'assets/manulife-epos-theme/images/save.svg',
  },
  {
    name: 'rename', 
    url: 'assets/manulife-epos-theme/images/rename.svg',
  },
  {
    name: 'copy', 
    url: 'assets/manulife-epos-theme/images/copy.svg',
  },
  {
    name: 'delete', 
    url: 'assets/manulife-epos-theme/images/delete.svg',
  },
  {
    name: 'create', 
    url: 'assets/manulife-epos-theme/images/create.svg',
  },
  {
    name: 'male', 
    url: 'assets/manulife-epos-theme/images/male.svg',
  },
  {
    name: 'female', 
    url: 'assets/manulife-epos-theme/images/female.svg',
  },
  {
    name: 'backspace', 
    url: 'assets/manulife-epos-theme/images/backspace.svg',
  },
  {
    name: 'draggable', 
    url: 'assets/manulife-epos-theme/images/draggable.svg',
  },
  {
    name: 'rounded_plus', 
    url: 'assets/manulife-epos-theme/images/rounded_plus.svg',
  },
  {
    name: 'rounded_minus', 
    url: 'assets/manulife-epos-theme/images/rounded_minus.svg',
  },
  {
    name: 'warning', 
    url: 'assets/manulife-epos-theme/images/warning.svg',
  },
  
];

@Injectable()
export class IconSvgRegistry {
  namespace = "epos";
  iconlist = IconSvgRegistryList;
  constructor(
      @Optional() config: IconSvgRegistryConfig,
      private iconRegistry: MatIconRegistry, 
      private sanitizer: DomSanitizer,
    ) {
      this.iconlist.forEach(element => {
        iconRegistry.addSvgIconInNamespace(
          this.namespace,
          element.name,
          sanitizer.bypassSecurityTrustResourceUrl(element.url));
      });

      if (config) { 
        config.icons.forEach(element => {
          iconRegistry.addSvgIconInNamespace(
            this.namespace,
            element.name,
            sanitizer.bypassSecurityTrustResourceUrl(element.url));
        });
      }
  }

}
