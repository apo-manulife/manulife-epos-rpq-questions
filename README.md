# manulife-epos-rpq-questions

###### Author: Apo Kong


##### Installations
###### Gulp + Cordova + Angular5 + typescript
- npm install -g gulp cordova @angular/cli typescript ios-deploy
###### NPM install
- npm install


##### Start
###### build www/ files with Anugluar
- ng build
###### build iOS files with Cordova
- cordova platform add ios
- cordova build


##### Run
###### Emulate in iPad simulator

    $ gulp build

###### Release

    $ gulp release



### License
- Apo Kong <apokong@gmail.com>
- ©2018 (ISC).